% !TEX root = __main.tex

\chapter{Constructing $n \times n$ Coupler from $2 \times 2$ Couplers}
\label{chap:coupler}
%%
%%
In this chapter I'm going to talk about mathematics coming up form practical needs. In experiment 
of fiber lasers, we needed to construct $n$-input-to-$n$-output, equally-distributing optical 
coupler but we had only $2\times2$ couplers on hand. Then how do you hook up 2x2 couplers and get 
the $n \times n$ coupler you want?  Here I'll discuss (1) whether an arbitrary $n \times n$ 
coupler may be constructed from $2 \times 2$ couplers and (2) provide an algorithm when (1) is 
true. It will turn out the problem is more complex than it appears.


%% ============================================================
%%        Building Blocks
%% ============================================================
\section{Building Blocks}
%
We assume field in a fiber is represented by a complex scalar. Fields in $n$ isolated fibers at a 
given cross-section point is thus given by a $n$ complex vector. Field at one cross section is 
related to a field at another cross sectiion via a transfer matrix. We have two types of transfer 
matrices: spatial translation $\mathcal{T}$ along fibers and $2 \times 2$ coupler $\mathcal{C}_{i 
j}$ which mixes light in $i$-th and $j$-th fibers. They are only building blocks consisting a 
transfer matrix.
%
Translation $\mathcal{T}$ is represented by $n$-by-$n$ diagonal matrix which takes $n$ parameters 
$\beta_i$ ($i=1, ..., N$) each of which represent phase shift to field in a fiber.
\[
\mathcal{T} (\beta _1, ...,\beta _n)
=
\text{diag} (e^{i \beta_1}, ..., e^{i \beta_n})
= 
\begin{pmatrix}[0.6]
    e^{i \beta_1} &           &  0     \\
                  &   \ddots  &        \\
            0     &           &  e^{i \beta_n}
\end{pmatrix}
\]
%
%
Different amount of phase shift occurs because optical path lengths are 
uncontrolled and can be mismatched. 
%
$2 \times 2$ coupler $\mathcal{C}_{i j}$ takes one parameter $\alpha$ which 
determines the ratio of mixing. 
%
%
\[
\mathcal{C}_{i j}(\alpha)
=
\begin{pmatrix}[0.6]
 1 &   &   &   &   &   \\
   & \ddots &   &   &   &   \\
   &   & \cos  \alpha  &   & i \sin  \alpha  &   \\
   &   &   & 1 &   &   \\
   &   & i \sin  \alpha  &   & \cos  \alpha  &   \\
   &   &   &   &   & 1 \\
\end{pmatrix}
\]
%
When a coupling matrix was calculated in Section~\ref{sec:coupler}, another parameter existed as a 
multiplicative phase factor. That factor is omitted here because the omitted phase factor is 
immediately recovered from product of $\mathcal{C}$ and $\mathcal{T}$. Fibers and couplers are 
assumed lossless so $\mathcal{T}$ and $\mathcal{C}$ are unitary matrices.


%% ============================================================
%%          Covering Problem
%% ============================================================
%
\section{Covering Problem}
%
%%--------------------------------------
%%          Problem Statement
\subsection{Problem Statement}
Our original fiber optics problem is to construct an equally-mixing $n\times n$ coupler from $2 \times 2$ couplers. Such coupler $\mathcal{M}$ is an unitary matrix of the form
%
\[
\mathcal{M}_{pq} = \frac{1}{\sqrt{n}} \exp (i \theta _{pq})
\]
%
where $p$ and $q$ are row and column indices of the matrix $(p,q = 1,...,n)$. 
Unitary property of $\mathcal{M}$ requires conditions among phases $\theta _{pq}$.
%
\[
  \frac{1}{n} \sum_{i=1}^n \exp (i \theta_{ip} - i \theta_{iq} ) = \delta_{pq}
\]
%
%
where $\delta_{pq}$ is a Kronecker delta. The condition holds for $p=1,...,n$ and $q=1,...,n$ so 
there are $\frac{1}{2}n(n+1)$ equations for parameters $\theta _{pq}$. 
 Our specific problem is,
\begin{itemize}
    \item Is it possible to express the matrix $\mathcal{M}$ as product of $\mathcal{T}$'s and $\mathcal{C}$'s? 
    \item If yes, what is an algorithm to find the representation?
\end{itemize}
%
To attack the first problem we investigate mathematical structure by asking the
 restatement of the question: ``Are every SU(n) matrices accessible by operations of 
 $\mathcal{T}$'s and $\mathcal{C}$'s?''



%%--------------------------------------
%%          Carton's Theorem
\subsection{Cartan's Covering Theorem}
\label{sub:cartan_theorem}
%
%
It is always possible to map a Lie algebra onto its Lie group with a products of exponential 
mappings applied to noncompact and compact Lie algebras (Cartan's theorem?) 
~\cite{GilmoreTextbook}. Since we are going to deal with only compact Lie algebra, 
$\mathfrak{su}$(n) with $n \geq 2$, its exponential mapping is onto the SU(n) group. With this 
onto relationship from Lie algebra to Lie group, our task is to find if Lie algebra is fully 
spanned.~\footnote{Since SU(n) is simply connected for $n\geq 2$, isomorphic Lie algebra give 
isomorphic (unique) Lie group and global re-parameterizations between Lie algebras exist.} So what 
we need to do is checking if the product of $\mathcal{T}$'s and $\mathcal{C}$'s are composed of 
fully-spanned Lie algebra.



%%--------------------------------------
%%          Disentangling in SU(n)?
\subsection{Disentangling in SU(n)?} % (fold)
\label{sub:disentangling}
%
%
When exponential map of Lie algebra is onto the group, product of exponentials is also onto the group with reparametralization $(a_1,a_2,...) \to (b_1,b_2,...)$. Is it also true in SU(n)?
%
\[
    \exp \left(\sum _i a_i \lambda _i \right) \overset{?}{=} \prod _i \exp (b_i \lambda_i)
\]
%
This is true for creation and annihilation operators $a\dagger$ and $a$
 where commutation relation $[a\dagger,a] = I$ holds.




%%--------------------------------------
%%          Case: n = 2
\subsection{Case: $n=2$ Coupler}
%
When coupling $n=2$ fibers, coupler and translation operators are 
$2\times 2$ matrices, respectively.
%
\begin{align*}
    \mathcal{T}(\beta) &= 
        \begin{pmatrix}[0.6]
            e^{i \beta } & 0 \\
            0            & e^{-i \beta }
        \end{pmatrix} \\
    \mathcal{C}(\alpha ) &=
        \begin{pmatrix}[0.6]
            \cos  \alpha    & i \sin  \alpha  \\
            i \sin  \alpha  & \cos  \alpha
        \end{pmatrix}
\end{align*}
%
Here translation $\mathcal{T}$ has only one parameter because multiplicative constant is 
unimportant and it may be included later if necessary. Generators of $\mathcal{T}$ and 
$\mathcal{C}$ are computed from derivative with respect to its parameter.
%
\begin{align*}
    \left. \frac{\partial \mathcal{C}(\alpha )}{\partial \alpha } \right|_{ \alpha = 0}
    &= 
    \begin{pmatrix}[0.6]
        0 & i \\
        i & 0
    \end{pmatrix}
    = u_1 = i \sigma _1  \\
%%
    \left. \frac{\partial \mathcal{T}(\beta )}{\partial \beta } \right|_{ \beta =0}
    &=
    \begin{pmatrix}[0.6]    
        i & 0 \\
        0 & -i
    \end{pmatrix}
    = u_3 = i \sigma _3
\end{align*}
%
where $u_1$ and $u_3$ are (one choice of) bases in Lie algebra $\mathfrak{su}(2)$ and $\sigma _1$ 
and $\sigma _3$ are Pauli matrices. Reversing the linearlization process, finite transformations 
are recovered by exponentiating their generators: $\mathcal{C}(\alpha ) = \exp (\alpha  u_1)$ and 
$\mathcal{T}(\beta )=\exp (\beta  u_3)$. Our problem is now analogous to construction of an 
arbitrary rotation in SO(3) with rotations along $x$ and $z$ axes. As well-known in spin-1/2 
system~\cite{SakuraiTextbook}, arbitrary SU(2) transformation may be achieved in the same way as 
SO(3) specifying three Euler angles, say, in $zxz$ order (with space-fixed axes).
%
\[
\mathcal{D}(\alpha, \beta, \gamma ) 
    = \exp (\alpha  u_3) \exp (\beta  u_1) \exp (\gamma  u_3)
    = \mathcal{T}(\alpha) \mathcal{C}(\beta) \mathcal{T}(\gamma) 
\]
%
Cartan's theorem enables us to check the coverage of SU(2). If $\mathcal{D}$ covers the entire Lie 
group, it should have a representation by exponential map of the fully-spanned Lie algebra 
$\mathfrak{su}(2)$.
%
\[
    \mathcal{D}(a,b,c) \overset{?}{=} \exp (a u_1 + b u_2 + c u_3)
\]
%
With the help of Baker-Campbell-Hausdorff (BCH) formula, 
Euler angles $(\alpha ,\beta ,\gamma )$ are related with parameters $(a,b,c)$. 
%
\begin{align*}
    \exp (\alpha  u_3)\exp (\beta  u_1)\exp (\gamma  u_3)
    &= \exp \left( \alpha  u_3 + \beta  u_1 + \gamma  u_3 
    + \frac{\alpha \beta }{2} [u_3, u_1] 
    + \frac{\beta  \gamma}{2} [u_1, u_3]
    + \frac{\gamma \alpha}{2} [u_3, u_3] +... \right) \\
    &= \exp \left(\beta u_1 + \beta (\alpha +\gamma ) u_2 + (\alpha +\gamma ) u_3 
        + O(\alpha ^2,\beta ^2,\gamma ^2) \right)
\end{align*}
%
Although closed form is not obtained from the series, we can confirm that the operation 
$\mathcal{T}(\alpha )\mathcal{C}(\beta )\mathcal{T}(\gamma )$ has Lie algebra spanned fully in 
$\mathfrak{su}(2)$. Thus, $\mathcal{T}(\alpha )\mathcal{C}(\beta )\mathcal{T}(\gamma )$ 
covers SU(2).



%%--------------------------------------
%%          Case: n = 3
\subsection{Case: $n=3$ Coupler}
%
When $n=3$ fibers are coupled, we can no longer restrict ourselves in SU(2) structure studied 
previously. This is because span of the generators is not contained in the span of 
$\mathfrak{su}(2)$. $\mathfrak{su}(2)$ in $3 \times 3$ matrix 
form is given from spin-1 system.
%
\begin{align*}
S_x
&= \frac{1}{2} (S_+ + S_-) = \frac{1}{\sqrt{2}}
\begin{pmatrix}[0.6]
 0 & 1 & 0 \\
 1 & 0 & 1 \\
 0 & 1 & 0 
\end{pmatrix} \\
%
S_y
&= \frac{1}{2i} (S_+-S_-) = \frac{i}{\sqrt{2}}
\begin{pmatrix}[0.6]
 0 & -1 & 0 \\
 1 & 0 & -1 \\
 0 & 1 & 0 
\end{pmatrix} \\
%
S_z
&= 
\begin{pmatrix}[0.6]
 1 & 0 & 0 \\
 0 & 0 & 0 \\
 0 & 0 & -1 
\end{pmatrix}
%
\end{align*}
%
%
And generators of $\{\mathcal{C}_{12}, \mathcal{C}_{31}, \mathcal{T}\}$ are
%
\begin{align*}
u_1 &= i 
\begin{pmatrix}[0.6]
 0 & 1 & 0 \\
 1 & 0 & 0 \\
 0 & 0 & 0 
\end{pmatrix},  &
%
u_6 &= i 
\begin{pmatrix}[0.6]
 0 & 0 & 0 \\
 0 & 0 & 1 \\
 0 & 1 & 0 
\end{pmatrix}, &
%
u_4 &= i 
\begin{pmatrix}[0.6]
 0 & 0 & 1 \\
 0 & 0 & 0 \\
 1 & 0 & 0 
\end{pmatrix}, \\
%
u_3 &= i 
\begin{pmatrix}[0.6]
 1 & 0 & 0 \\
 0 & -1 & 0 \\
 0 & 0 & 0 
\end{pmatrix},  &
%
u_8 &=\frac{i}{\sqrt{3}}
\begin{pmatrix}[0.6]
 1 & 0 & 0 \\
 0 & 1 & 0 \\
 0 & 0 & -2 
\end{pmatrix}  &
%
&
%
\end{align*}
%
%
where two independent parameters in $\mathcal{T}$ yield two generators $v_3$ and $v_8$. Here 
indices $\{ 1, 3, 4, 6, 8 \}$ are labeled for later purpose when Gell-Mann matrices are 
introduced. Obviously, dimension of the vector space spanned by $u_1 \otimes u_3 \otimes u_4 
\otimes u_6 \otimes u_8$ is higher than that of $S_x \otimes S_y \otimes S_z$. We need to search 
for a group larger than SU(2). A tool for constructing complementary vector space is a commutator 
$[u,v] = u v-v u$. From direct matrices calculation we obtain more generators
%
\begin{align*}
    [u_1, u_6] &= -u_5, &  [u_4,u_1] &= -u_7,  & [u_6,u_4] &= u_2,
\end{align*}
%
where
%
\begin{align*}
u_7 &=
    \begin{pmatrix}[0.6]
     0 & 0 & 0 \\
     0 & 0 & 1 \\
     0 & -1 & 0 
    \end{pmatrix},  &
%%
u_5 &=
    \begin{pmatrix}[0.6]
     0 & 0 & -1 \\
     0 & 0 & 0 \\
     1 & 0 & 0 
    \end{pmatrix}, &
%%    
u_2 &=
    \begin{pmatrix}[0.6]
     0 & 1 & 0 \\
     -1 & 0 & 0 \\
     0 & 0 & 0 
    \end{pmatrix}
\end{align*}
%
%
Thus, operators $u_i$ ($i=1,...,8)$ are closed under commutation. Indeed, our operators are 
isomorphic to Gell-Mann's choice of Lie algebra of $\mathfrak{su}(3)$\cite{ZeeTextbook}.
\begin{align*}
\lambda_1 &= 
\begin{pmatrix}[0.6]
 0 & 1 & 0 \\
 1 & 0 & 0 \\
 0 & 0 & 0 
\end{pmatrix},  &
%%
\lambda _2 &=
\begin{pmatrix}[0.6]
 0 & -i & 0 \\
 i & 0 & 0 \\
 0 & 0 & 0 
\end{pmatrix},  &
%%
\lambda _3 &=
\begin{pmatrix}[0.6]
 1 & 0 & 0 \\
 0 & -1 & 0 \\
 0 & 0 & 0 
\end{pmatrix}  &
%%
\lambda _4 &=
\begin{pmatrix}[0.6]
 0 & 0 & 1 \\
 0 & 0 & 0 \\
 1 & 0 & 0 
\end{pmatrix}, \\
%%
\lambda _5 &=
\begin{pmatrix}[0.6]
 0 & 0 & -i \\
 0 & 0 & 0  \\
 i & 0 & 0 
\end{pmatrix},  &
%%
\lambda _6 &=
\begin{pmatrix}[0.6]
 0 & 0 & 0 \\
 0 & 0 & 1 \\
 0 & 1 & 0 
\end{pmatrix},  &
%%
\lambda _7 &=
\begin{pmatrix}[0.6]
 0 & 0 & 0  \\
 0 & 0 & -i \\
 0 & i & 0 
\end{pmatrix},  &
%%
\lambda _8 &=
\frac{1}{\sqrt{3}}
\begin{pmatrix}[0.6]
 1 & 0 & 0 \\
 0 & 1 & 0 \\
 0 & 0 & -2 
\end{pmatrix} &
%%
\end{align*}
%
%
Now we look for a representation of the exponential map $\exp \left( \bigotimes_{i=1}^8 a_i \lambda _i \right)$ as a product of finite transformations. One way is to focus on its subgroup. $\mathfrak{su}(3)$ has subgroup $\mathfrak{su}(2)$ spanned by 
$\lambda_1 \otimes \lambda_2 \otimes \lambda_3$, 
$\lambda_4 \otimes \lambda_5 \otimes \left( \sqrt{3} \lambda_8 + \lambda_3 \right)$,
$\lambda_6 \otimes \lambda_7 \otimes \left( \sqrt{3} \lambda_8 - \lambda_3 \right)$.
%
And we have finite rotations 
$\mathcal{C}_{12} (\alpha ) = \exp (i \alpha  \lambda_1)$,
$\mathcal{C}_{31} (\alpha ) = \exp (i \alpha  \lambda_4)$,
$\mathcal{C}_{23} (\alpha ) = \exp (i \alpha  \lambda_6)$,
$\mathcal{T}(\beta, -\beta, 0) = \exp (i \beta  \lambda_3)$,
$\mathcal{T}(2 \beta, 0, -2 \beta ) = \exp \left( i \beta ( \sqrt{3} \lambda_8 + \lambda_3 ) \right)$,
and
$\mathcal{T}(0, 2 \beta, -2 \beta ) = \exp \left( i \beta ( \sqrt{3} \lambda_8 - \lambda_3 ) \right)$. 
%
Thus, each subgroup has analogous structure we have seen in $n=2$ case. Now the problem is whether there is a disentangling relation(*).
%
\begin{align*}
    & \exp \left(\sum _{i=1}^8  a_i \lambda _i \right) 
    \overset{?}{=} \\
    & \exp ( \alpha _1 \lambda _1 + \alpha _2 \lambda _2 + \alpha _3 \lambda _3 )
      \exp ( \beta_1  \lambda_4 + \beta_2 \lambda_5 + \beta_3 (\sqrt{3} \lambda_8 + \lambda_3) )
      \exp ( \gamma_1 \lambda_6 + \gamma_2 \lambda_7 + \gamma_3 (\sqrt{3} \lambda_8 - \lambda_3) )
\end{align*}
BCH formula seems to support this relation.



%%--------------------------------------
%%          Case: n = 4
\subsection{Case: $n=4$ Coupler}
%
Even in $n\geq 4$, covering problem may be discussed in the same manner as $n=2, 3$. The number of 
generators of SU(n) is $(n^2 - 1)$. And number of generators extracted from couplers and 
translations is $n(n-1)/2 + (n-1) = \frac{1}{2}(n-1)(n+2)$. Now problems are,
\begin{itemize}
    \item Can we always construct entire Lie algebra $\mathfrak{su}(n)$ with commutation relations?
    \item What is an algorithm for systematic construction of SU(n) from product of available operators?
\end{itemize}
\[
  \exp \left(\sum _{i=1}^{n^2 - 1}  a_i \lambda_i \right)
  \overset{?}{=}
  \prod_{i=1}^{n^2 - 1} \exp (b_i \lambda_i)
\]
There must be ways to carry this out systematically without matrices 
calculation but they are beyond my reach for now.



\subsection{Relation Between Two Different Parameterizations in SU(2)}
%
There are various ways to represent arbitrary SU(2) transformation. 
One way to cover SU(2) is via sequence of finite rotations, say, ``$zxz$-rotations''.
\begin{align*}
    \mathcal{D}_1 (\alpha, \beta, \gamma ) = 
    \begin{pmatrix}[0.6]
        e^{i \alpha } & 0 \\
        0 & e^{-i \alpha } 
    \end{pmatrix}
    %
    \begin{pmatrix}[0.6]
        \cos  \beta  & i \sin  \beta  \\
        i \sin  \beta  & \cos  \beta  
    \end{pmatrix}
    %
    \begin{pmatrix}[0.6]
        e^{i \gamma } & 0 \\
        0 & e^{-i \gamma } 
    \end{pmatrix}
    =
    \begin{pmatrix}[0.6]
        e^{i (\alpha +\gamma )} \cos  \beta  & i e^{i (\alpha -\gamma )} \sin  \beta  \\
        i e^{-i (\alpha -\gamma )} \sin  \beta  & e^{-i (\alpha +\gamma )} \cos  \beta  
    \end{pmatrix}
\end{align*}
%
%
On the other hand, exponential map of a general vector in Lie algebra 
$\mathfrak{su}(2)$ also gives an expression for the transformation.
%
\[
\mathcal{D}_2 (a, b, c)
=\exp (a u_1 + b u_2 + c u_3)=\exp \left(i 
\begin{pmatrix}[0.6]
 c     & a+i b \\
 a-i b & -c 
\end{pmatrix} \right)
=
\begin{pmatrix}[0.6]
 \cos  \phi +i n_z\sin \phi  & (i n_x + n_y) \sin \phi  \\
 (i n_x - n_y) \sin \phi     & \cos  \phi -i n_z \sin \phi  
\end{pmatrix}
\]
%
%
where $\phi \equiv \sqrt{a^2+b^2+c^2}$,
$(n_x, n_y, n_z) \equiv \frac{1}{\sqrt{a^2+b^2+c^2}}(a,b,c)$.
%
%
Now we have two different representations, one is with parameters 
$(\alpha, \beta, \gamma )$ and the other with $(a,b,c)$.  
From element-wise comparison of $\mathcal{D}_1=\mathcal{D}_2$, 
we obtain explicit relation.
\begin{align*}
\frac{a}{\sqrt{a^2+b^2+c^2}}\sin  \phi &=   \cos (\alpha - \gamma ) \sin  \beta  \\
\frac{b}{\sqrt{a^2+b^2+c^2}}\sin  \phi &= - \sin (\alpha - \gamma ) \sin  \beta  \\
\frac{c}{\sqrt{a^2+b^2+c^2}}\sin  \phi &= - \sin (\alpha + \gamma ) \sin  \beta
\end{align*}








%% ============================================================
%%        Constructions
%% ============================================================
%%
\section{Algorithm for Constructing Equally-mixing Coupler}
Now we try to construct $n \times n$ equally-mixing composite coupler $\mathcal{M}_n$ from 
$2\times 2$ couplers and translations. We may proceed along the prescriptions for SU(n) 
construction in $n=2$ and $n=3$ couplers. But for $n\geq 4$, only a heuristic approach is 
available for now. $\mathcal{M}_n$ has more symmetries (or constraints) than an arbitrary SU(n) so 
its representation requires less parameters than $(n^2 - 1)$, which makes the product presentation 
simple.




%----------------------------------
%       n = 2
\subsection{Transformation in SU(2): $n=2$ Coupler}
%
$2\times 2$ matrix representation of SU(2) is given by
%
\[ 
\mathcal{M}=
\begin{pmatrix}[0.6]
 \alpha  & -\bar{\beta } \\
 \beta  &   \bar{\alpha } 
\end{pmatrix}
\]
%
with $|\alpha |^2 + |\beta |^2 = 1$. Thus, an equally-mixing $2\times 2$ 
coupler $\mathcal{M}$ has two parameters $(x,y)$.
%
\[
\mathcal{M}(x,y)
=\frac{1}{\sqrt{2}}
\begin{pmatrix}[0.6]
 e^{i x} & -e^{-i y} \\
 e^{i y} & e^{-i x} 
\end{pmatrix}
\]
%
Now we use an explicit form of $\mathcal{D}(\alpha ,\beta ,\gamma )$, given in the last section in this Appendix, to represent $\mathcal{M}(x,y)$. Comparing two matrices, we obtain $(\alpha ,\beta ,\gamma )$.
%
\[
    (\alpha, \beta, \gamma) 
    = \left(\frac{x-y}{2} + \frac{\pi}{4}, \frac{\pi}{4}, \frac{x+y}{2} - \frac{\pi}{4} \right)
\]



%----------------------------------
%       n = 3
\subsection{Transformation in SU(3): $n=3$ Coupler}
%
An equally-mixing $3\times 3$ coupler is given by
%
\[
\mathcal{M} = \frac{1}{\sqrt{3}}
\begin{pmatrix}[0.6]
 e^{i \theta _{11}} & e^{i \theta _{12}} & e^{i \theta _{13}} \\
 e^{i \theta _{21}} & e^{i \theta _{22}} & e^{i \theta _{23}} \\
 e^{i \theta _{31}} & e^{i \theta _{32}} & e^{i \theta _{33}} 
\end{pmatrix}
\]
%
%
and special unitary property imposes $\mathcal{M}\dagger \mathcal{M} = I$ and
$|\mathcal{M}|=1$. We know that any unitary matrix may be expressed by 
%
\[
\mathcal{D}_{23} (\alpha_3, \beta_3, \gamma_3) 
\mathcal{D}_{31} (\alpha_2, \beta_2, \gamma_2)
\mathcal{D}_{12} (\alpha_1, \beta_1, \gamma_1)
\]
%
where $\mathcal{D}_{ij}(\alpha ,\beta ,\gamma )$ is a SU(2) rotation 
involving connection between $i$-th and $j$-th fiber. 
In case of equally-mixing matrix $\mathcal{M}_3$, we only need to 
choose $(\beta _3,\beta _2,\beta _1) = (\pi /4, \cos ^{-1} (\sqrt{2/3}), \pi /4)$.
Then,
%
\begin{align*}
    \text{Abs}(\mathcal{M}) = \frac{1}{\sqrt{3}}
    \begin{pmatrix}[0.6]
        1        &  1        &  1 \\
        \abs{g}  &  \abs{g}  &  1 \\
        \abs{g}  &  \abs{g}  &  1
    \end{pmatrix}
\end{align*}
%
with 
%
\[
  g = \frac{1}{2} \left( \sqrt{3} e^{i (2 a_1 + a_2)} - i e^{i (c_2 + 2 c_3)}  \right).
\]
%
As long as the angle $(2a_1 + a_2)$ is different from $(c_2 + 2 c_3)$ by 0 or $\pi$,
the matrix $\mathcal{M}$ becomes equal mixing.  Choosing the simplest configuration 
$(\alpha_1, \alpha_2, \alpha_3, \gamma_1, \gamma_2, \gamma_3) = (0,0,0,0,0,0)$,
we have $\mathcal{M}$ of the form
%
\[
\mathcal{M}_3
=\mathcal{F}(1,2,3)
=\frac{1}{\sqrt{3}}
\begin{pmatrix}[0.6]
 1 & (-1)^{2/3} & (-1)^{5/6} \\
 i & (-1)^{2/3} & (-1)^{2/3} \\
 i & i & 1 
\end{pmatrix}
\]
%
%
where 
%
\[
\mathcal{F}(i,j,k) \equiv 
\mathcal{C}_{ki} \left(\frac{\pi}{4}\right)
\mathcal{C}_{jk} \left(\cos^{-1} (\sqrt{2/3})\right)
\mathcal{C}_{ij} \left(\frac{\pi}{4}\right).
\]






%----------------------------------
%       n = 4
\subsection{Algorithm for Constructing Equally-mixing $n=4$ Coupler}
%
For $n \geq 4$, the representation requires at most $(n^2-1)$ finite transformations. 
But symmetries in $\mathcal{M}_4$ enables us to construct it more economically. 
Heuristic observation based on its network geometry gives us following.
%
\begin{figure}[h!]
    \centering
    \includegraphics[width=5cm]{pdf/figure_compositecoupler_n4.pdf}
    \caption{Construction of 4x4 equally-mixing coupler.}
\end{figure}
%
%
\begin{align*}
\mathcal{M}_4 
= 
\mathcal{C}_{41} \left(\frac{\pi}{4}\right)
\mathcal{C}_{23} \left(\frac{\pi}{4}\right)
\mathcal{C}_{34} \left(\frac{\pi}{4}\right)
\mathcal{C}_{12} \left(\frac{\pi}{4}\right)
=\frac{1}{2}
\begin{pmatrix}[0.6]
 1 & i & -1 & i \\
 i & 1 & i & -1 \\
 -1 & i & 1 & i \\
 i & -1 & i & 1 
\end{pmatrix}
\end{align*}.






%----------------------------------
%       n = 5, 7
\subsection{Case: $n=5, 7$}
%
When $n=5$ and $n=7$, heuristic geometrical interpretation does not work
 due to the subtlety of interference. More systematic approach is needed. 



%----------------------------------
%       n = 6
\subsection{Algorithm for Constructing Equally-mixing $n=6$ Coupler}
%
From heuristic observation that $n=6$ connections as combination of 
two equally-mixed triangles, we could obtain an equally-mixing coupler setup.
%
\begin{figure}[h!]
    \centering
    \begin{subfigure}[h!]{5cm}
    \includegraphics[width=5cm]{pdf/figure_compositecoupler_n6a.pdf}
    \end{subfigure}
    \qquad \qquad
    \begin{subfigure}[h!]{5cm}
    \includegraphics[width=5cm]{pdf/figure_compositecoupler_n6b.pdf}
    \end{subfigure}
    \caption{Two ways of construction of 6x6 coupler}
\end{figure}
%
\[
\mathcal{M}_6 = 
\mathcal{C}_{12} \left(\frac{\pi}{4}\right)
\mathcal{C}_{34} \left(\frac{\pi}{4}\right)
\mathcal{C}_{56} \left(\frac{\pi}{4}\right)
\mathcal{F}(1,3,5)\mathcal{F}(2,4,6)
\]
where $\mathcal{F}$ is a function defined in construction of $n=3$.
which produces an equally-mixing $6 \times 6$ coupler
\[
\mathcal{M}_6
= \frac{1}{\sqrt{6}}
\begin{pmatrix}[0.6]
 \frac{1}{2} \left(-i+\sqrt{3}\right) & \frac{1}{2} \left(1+i \sqrt{3}\right) &
  i & -1 & 
 \frac{1}{2} i \left(i+\sqrt{3}\right) & \frac{1}{2} \left(-i-\sqrt{3}\right) \\
 %
 \frac{1}{2} \left(1+i \sqrt{3}\right) & \frac{1}{2} \left(-i+\sqrt{3}\right) &
  -1 & i & 
 \frac{1}{2} \left(-i-\sqrt{3}\right) & \frac{1}{2} i \left(i+\sqrt{3}\right) \\
 %
 \frac{1}{2} i \left(i+\sqrt{3}\right) & \frac{1}{2} \left(-i-\sqrt{3}\right) &
  1 & i & 
 \frac{1}{2} \left(i-\sqrt{3}\right) & -\frac{1}{2} i \left(-i+\sqrt{3}\right) \\
 %
 \frac{1}{2} \left(-i-\sqrt{3}\right) & \frac{1}{2} i \left(i+\sqrt{3}\right) &
  i & 1 &
 -\frac{1}{2} i \left(-i+\sqrt{3}\right) & \frac{1}{2} \left(i-\sqrt{3}\right) \\
 i & -1 & i & -1 & 1 & i \\
 -1 & i & -1 & i & i & 1 
\end{pmatrix}
\]



%----------------------------------
%       n = 8
\subsection{Algorithm for Constructing Equally-mixing $n=8$ Coupler}
%
Viewing the connections as combination of two equally-mixed squares, we may construct $\mathcal{M}_8$ heuristically.
%
%
\begin{figure}[h!]
    \centering
    \includegraphics[width=8cm]{pdf/figure_compositecoupler_n8.pdf}
    \caption{Construction of 8x8 equally-mixing coupler.}
\end{figure}
%
\begin{align*}
    \mathcal{M}_8  &=
    \left[
        \mathcal{C}_{15} \left(\frac{\pi}{4}\right)
        \mathcal{C}_{26} \left(\frac{\pi}{4}\right)
        \mathcal{C}_{37} \left(\frac{\pi}{4}\right)
        \mathcal{C}_{48} \left(\frac{\pi}{4}\right)
    \right] \\
    & \qquad \times
    \left[
        \mathcal{C}_{67} \left(\frac{\pi}{4}\right)
        \mathcal{C}_{85} \left(\frac{\pi}{4}\right)
        \mathcal{C}_{56} \left(\frac{\pi}{4}\right)
        \mathcal{C}_{78} \left(\frac{\pi }{4}\right)
    \right] \;
    \left[
        \mathcal{C}_{23} \left(\frac{\pi}{4}\right)
        \mathcal{C}_{41} \left(\frac{\pi}{4}\right)
        \mathcal{C}_{12} \left(\frac{\pi}{4}\right)
        \mathcal{C}_{34} \left(\frac{\pi}{4}\right)
    \right]
\end{align*}


\[
    \mathcal{M}_8 = \frac{1}{\sqrt{8}}
    \begin{pmatrix}[0.6]
        1 & i & -1 & i & i & -1 & -i & -1 \\
        i & 1 & i & -1 & -1 & i & -1 & -i \\
        -1 & i & 1 & i & -i & -1 & i & -1 \\
        i & -1 & i & 1 & -1 & -i & -1 & i \\
        i & -1 & -i & -1 & 1 & i & -1 & i \\
        -1 & i & -1 & -i & i & 1 & i & -1 \\
        -i & -1 & i & -1 & -1 & i & 1 & i \\
        -1 & -i & -1 & i & i & -1 & i & 1 
    \end{pmatrix}
\]



%----------------------------------
%       n = 9
\subsection{Case: $n=9$}
%
Considering the connections as combination of three equally-mixed squares, we can get $\mathcal{M}_9$ heuristically.
\[
\mathcal{M}_9 =
\mathcal{F}(1,2,3) \mathcal{F}(4,5,6)
\mathcal{F}(7,8,9) \mathcal{F}(1,4,7)
\mathcal{F}(2,5,8) \mathcal{F}(3,6,9)
\]
%
The explicit form of $\mathcal{M}_9$ is suppressed just because it's too lengthy.
% \[\mathcal{M}_9=\frac{1}{3}
% \begin{pmatrix}[0.6]
%  \frac{1}{2} \left(1-i \sqrt{3}\right) & \frac{1}{2} \left(1+i \sqrt{3}\right) & i & \frac{1}{2} \left(1+i \sqrt{3}\right) & -1 & \frac{1}{2} \left(-i-\sqrt{3}\right)
% & i & \frac{1}{2} \left(-i-\sqrt{3}\right) & \frac{1}{12} \left(-3 i+\sqrt{3}\right)^2 \\
%  i & \frac{1}{2} \left(-i+\sqrt{3}\right) & \frac{1}{2} i \left(i+\sqrt{3}\right) & \frac{1}{2} \left(-i-\sqrt{3}\right) & i & -\frac{1}{2} i \left(-i+\sqrt{3}\right)
% & \frac{1}{12} \left(-3 i+\sqrt{3}\right)^2 & \frac{1}{2} i \left(i+\sqrt{3}\right) & -i \\
%  \frac{1}{2} \left(1+i \sqrt{3}\right) & \frac{1}{2} \left(1+i \sqrt{3}\right) & \frac{1}{2} \left(-i+\sqrt{3}\right) & -1 & -1 & i & \frac{1}{2}
% \left(-i-\sqrt{3}\right) & \frac{1}{2} \left(-i-\sqrt{3}\right) & \frac{1}{2} i \left(i+\sqrt{3}\right) \\
%  i & \frac{1}{2} \left(-i-\sqrt{3}\right) & \frac{1}{12} \left(-3 i+\sqrt{3}\right)^2 & \frac{1}{2} \left(-i+\sqrt{3}\right) & i & \frac{1}{2} i
% \left(i+\sqrt{3}\right) & \frac{1}{2} i \left(i+\sqrt{3}\right) & -\frac{1}{2} i \left(-i+\sqrt{3}\right) & -i \\
%  \frac{1}{12} \left(-3 i+\sqrt{3}\right)^2 & \frac{1}{2} i \left(i+\sqrt{3}\right) & -i & \frac{1}{2} i \left(i+\sqrt{3}\right) & 1 & \frac{1}{2}
% \left(i-\sqrt{3}\right) & -i & \frac{1}{2} \left(i-\sqrt{3}\right) & \frac{1}{2} \left(1-i \sqrt{3}\right) \\
%  \frac{1}{2} \left(-i-\sqrt{3}\right) & \frac{1}{2} \left(-i-\sqrt{3}\right) & \frac{1}{2} i \left(i+\sqrt{3}\right) & i & i & 1 & -\frac{1}{2} i
% \left(-i+\sqrt{3}\right) & -\frac{1}{2} i \left(-i+\sqrt{3}\right) & \frac{1}{2} \left(i-\sqrt{3}\right) \\
%  \frac{1}{2} \left(1+i \sqrt{3}\right) & -1 & \frac{1}{2} \left(-i-\sqrt{3}\right) & \frac{1}{2} \left(1+i \sqrt{3}\right) & -1 & \frac{1}{2} \left(-i-\sqrt{3}\right)
% & \frac{1}{2} \left(-i+\sqrt{3}\right) & i & \frac{1}{2} i \left(i+\sqrt{3}\right) \\
%  \frac{1}{2} \left(-i-\sqrt{3}\right) & i & -\frac{1}{2} i \left(-i+\sqrt{3}\right) & \frac{1}{2} \left(-i-\sqrt{3}\right) & i & -\frac{1}{2} i \left(-i+\sqrt{3}\right)
% & \frac{1}{2} i \left(i+\sqrt{3}\right) & 1 & \frac{1}{2} \left(i-\sqrt{3}\right) \\
%  -1 & -1 & i & -1 & -1 & i & i & i & 1 
% \end{pmatrix}
% \]
\begin{figure}[h!]
    \centering
    \includegraphics[width=5cm]{pdf/figure_compositecoupler_n9.pdf}
    \caption{Construction of 9x9 equally-mixing coupler.}
\end{figure}



%----------------------------------
%       n  > 10 
\subsection{$n \geq 10$}
The heuristic algorithm may be extended further and applicable to $n=12, 16, 18, 24, 32, 36, ...$. 
The algorithm is applicable to $n = 2^i 3^j$ where $i$ and $j$ must be numbers expressed by $i = 
2^{i'} 3^{j'}$ and $j= 2^{i''} 3^{j''}$, respectively, where $i', j'$, $i'', j''$ must be also of 
the form. Recursive structure appears, as in figure \ref{fig:compositecoupler_general}, simply 
because we are applying the rules for the either $n=2$ or $n=3$ systems repeatedly~\footnote{The 
procedure is pretty similar to algorithmic construction of hypercube graph.}
%%
\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{pdf/figure_compositecoupler_general.pdf}
    \caption{Recursive construction of equally-mixing coupler.}
    \label{fig:compositecoupler_general}
\end{figure}


