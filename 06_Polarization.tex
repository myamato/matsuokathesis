% !TEX root = __main.tex

% Cold-cavity setup (no gain included) of $N=2$ coupled lasers is 
% considered from wave equation model. Purpose of this study is to
% see if we can reproduce experimental observation that output 
% efficiency is (in)dependent of polarization controllers. Our argument
% and numerical computation fully reproduced the experimental results
% by considering birefringence in fiber and lowest-loss mode selection.
% The perfect beam combining is achieved by canceling out 
% the intrinsic birefringence effect in one of the fiber arms.
%
%
\chapter{Polarization in Coherent Beam Combining}
\label{chap:polarization}
%
%%===========================================================
%%           Background / Experiment
%%===========================================================
%%
\section{Background}
\subsection{Is Manual Adjustment of Polarization Necessary?}
As Cao \emph{et al} pointed out\cite{Cao2009polarization}, importance of polarization adjustments 
in passive beam combining systems is controversial.  Many researchers report\cite{Simpson2002, 
Sabourdy2003, Shirakawa2002, Shirakawa2005} coherent combining in $N$ array with installation of 
$(N-1)$ or more\footnote{Simpson\cite{Simpson2002} inserted a polarization controllers for each of 
two arms in $N=2$ coupled fiber setup and reports that ``a single adjustment is sufficient for 
data taken over several hours'' and observed degradation of coherence down to 50\% combining 
efficiency by manipulating polarizers. Sabourdy\cite{Sabourdy2003} inserted a single polarization 
control in $N=2$ system and reports that ``The combining efficiency optimized by adjusting the 
polarization controller at a given emission wavelength does not require additional adjustments at 
other wavelengths.''} polarization controllers while others claim\cite{Minden2004, 
Bruesselbach2005} that polarization controls were unnecessary due to the self-organization of the 
system.  Then, Cao concluded from their experiments that polarization adjustments are unnecessary 
in Michelson-interferometer-type linear cavity (used in Shirakawa, Minden) but necessary in 
Mach-Zehnder-type configurations (used in Sabourdy).



\subsection{Experimental Observation}
 Our unpublished experiments in two coupled laser in Michelson-interferometer 
 cavity suggests that \emph{one} polarization adjustment is necessary but in a strange manner. 

 In general, adjustment of polarization in one fiber arm is required to obtain coherent combining. 
 As depicted in upper two graphs in Figure \ref{fig:willray_coherence_vs_polarization}, perfect 
 addition of light ($\omega=1$ in the graphs) is achieved when one of polarization controller is 
 tuned. However, when the other polarization controller is set to certain value giving perfect 
 addition, manipulation of the controller attached to one arm did not make any degradation to the 
 coherence. (Bottom graph in Figure \ref{fig:willray_coherence_vs_polarization}.)
%
%
\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{pdf/figure_willray_coherence_vs_polarization.pdf}
    \caption{Addition efficiency is plotted against change in a polarization controller setting.  
    When a polarization controller of one arm $\theta_2$ is adjusted to the optimal, coherence 
    persists even if the other polarization control $\theta_1$ is manipulated.}
    \label{fig:willray_coherence_vs_polarization}
\end{figure}
%
We are going to explain the experimental result and clarify the necessity of polarization control 
in coherent beam combining with theoretical tools and results developed in Chapter 
\ref{chap:dynamics}.




%%===========================================================
%%           Geometry of Polarized Light
%%===========================================================
\section{Geometry of Polarized Light}
%
%-------------------------------------------
\subsection{Jones Vector and Bloch Sphere --Geometry Juggling--}
%
Polarization of light is represented by Jones vector $\vect{x}$, 
a point in $3$-sphere $S^3 = \left\{ \vect{x} \in \mathbb{C}^2, \|\vect{x}\| = 1 \right\}$.
In component form, 
%
\begin{align}
    \vect{x} = \left(E_x, E_y\right)
             = e^{ i \gamma } \left(\cos \frac{\theta}{2},  e^{ i \varphi } \sin \frac{\theta}{2} 
             \right)
     \label{eq:jones_vector}
\end{align}
%
where $\theta \in [0, \pi ]$ and $\varphi \in [0,2\pi )$. 
$e^{ i \gamma }$ is overall phase factor and does not make any physical difference. 
Disregarding the factor, we represent a state in projected space 
$\vect{x} \to \vect{x'} \in  S^3/\mathrm{U}(1)$ where U(1) is unitary group due to 
$e^{ i \gamma }$ factor and $S^3/\mathrm{U}(1)$ is quotient.  
It is known that $S^3/\mathrm{U}(1)$ is isomorphic to 2-sphere $S^2$ and 
$S^3$ is isomorphic to SU(2).

Jones vector is mathematically identical to a quantum state $\ket{\psi}$ in spin-1/2 system.
%
\begin{align*}
    \ket{\psi} = \cos \frac{\theta }{2} \ket{0}  + e^{ i \varphi } \sin \frac{\theta }{2} \ket{1}.
\end{align*}
%
Just as in the case of Jones vector, phase factor is disregarded and $\ket{\psi}$ and $\ket{\phi} 
= e^{i \gamma} \ket{\psi}$ are treated physically equivalent.

Geometrical representation of $\vect{x'} \in S^2 \subset \mathbb{R}^3$ is called ``Poincaré 
sphere'' in optics and ``Bloch sphere'' in quantum two-state systems\cite{BornWolfTextbook}.  As 
the geometry suggests, the sphere deals with reduced space by taking off $e^{ i \gamma }$ factor 
instead of the original $\mathrm{SU} (2) \cong S^3$.

This sphere can accommodate mixed state (partially-polarized light) inside the sphere but we focus 
on the surface assuming pure polarization.  With choice of $x y z$ axes in Figure \ref{}, Jones 
vector $\vect{x}$ may be represented in Cartesian coordinate $\mathbb{R}^3$.
%
\begin{align*}
    x &= \sin  \theta  \cos  \varphi = 2 \, \mathrm{Re} (E_x^* E_y) \\
    y &= \sin  \theta  \sin  \varphi = 2 \, \mathrm{Im} (E_x^* E_y) \\
    z &= \cos \theta = |E_x|^2 - |E_y|^2
\end{align*}
%
Rotations about axes represented in $\mathrm{SU}(2)$ is
%
\begin{align}
    R_x(a) &= 
    \begin{pmatrix}
         \cos  \frac{a}{2}    & -i \sin  \frac{a}{2} \\
         -i \sin  \frac{a}{2} & \cos  \frac{a}{2} 
    \end{pmatrix},   
    &\quad %----- 
    R_y(b) &= 
    \begin{pmatrix}
        \cos  \frac{b}{2}  & -\sin  \frac{b}{2} \\
        \sin  \frac{b}{2} & \cos  \frac{b}{2} 
    \end{pmatrix}, 
    &\quad %-----
    R_z(c) &= 
        \begin{pmatrix}
            e^{-i c/2}  &  0         \\
            0            & e^{ i c/2} 
        \end{pmatrix}
        \label{eq:three_dimensional_rotation}
\end{align}
%
and they are actually exponentiation of Pauli matrices 
$R_i(\theta) = \exp (-i \sigma _i\theta  /2 )$.
%
\begin{align*}
    \sigma_x &=
    \begin{pmatrix}
        0 & 1 \\
        1 & 0 
    \end{pmatrix}, 
    %-----
    \quad &
    %-----
    \sigma_y &=
    \begin{pmatrix}
        0 & -i \\
         i & 0 
    \end{pmatrix},
    %-----
    \quad &
    %-----
     \sigma_z &= 
    \begin{pmatrix}     
        1 & 0 \\
        0 & -1
    \end{pmatrix}.        
\end{align*}
%
Although Jones vector $\vect{x}$ essentially lies in $S^2 \cong \mathrm{SU}(2)/\mathrm{U}(1)$, 
its transformation property is discussed in $S^3 \cong \mathrm{SU}(2)$ and its graphical 
representation is in $\mathbb{R}^3$ because of mathematical neatness in these super sets. 
But we have to be aware of the working geometry because the juggling sometimes introduces 
confusion.





%-------------------------------------------
\subsection{Birefringence}

Now, we consider how Jones vector $\vect{x}$ is transformed by birefringence. Birefringence 
introduces axis-dependence of phase propagation along a fiber and it is described by an operator 
$\mathcal{P}$: $S^3\to S^3$.  $\mathcal{P}$ is expressed with two parameters $(\theta, \gamma)$, 
where $\theta$ is angle of principal axes in cross section and $\gamma$ determines phase 
separation between two axes. Polarization controller, depicted in figure 
\ref{fig:polarization_controller_diagram}, works exactly the same manner and described by 
$\mathcal{P}$ with user-tunable parameters. In explicit form,
%
\begin{align*}
    \mathcal{P} (\theta, \gamma) 
    &= 
        \begin{pmatrix}
            \cos \frac{\theta}{2} &  -\sin \frac{\theta}{2} \\
            \sin  \frac{\theta}{2} & \cos \frac{\theta}{2} 
        \end{pmatrix}
        %%
        \begin{pmatrix}
             e^{- i \gamma } & 0                   \\
                              0 & e^{ i \gamma } 
        \end{pmatrix}
        %%
    \begin{pmatrix}
          \cos \frac{\theta }{2}  &    \sin \frac{\theta }{2} \\
            -\sin  \frac{\theta }{2} &    \cos \frac{\theta }{2}
        \end{pmatrix} \\
    %%-----------
    &= 
    \begin{pmatrix}
          \cos  \gamma -i \sin \gamma \cos \theta  & -i \sin  \gamma  \sin  \theta  \\
                          -i \sin  \gamma  \sin  \theta  & \cos \gamma + i \sin  \gamma  \cos  \theta 
    \end{pmatrix} \\
    %%
    &=
    \exp (- i \sigma_y \theta ) \exp (-i \sigma_z \gamma ) \exp ( i \sigma_y \theta )
\end{align*}
%
with $\theta \in [0, \pi ]$ and $\gamma \in (- \pi, \pi ]$. $\mathcal{P}$ is a symmetric matrix 
$\mathcal{P}^T = \mathcal{P}$ and it reflects the physical property that changes in polarization 
of the counter-propagating light are the same in the birefringent material, or polarization 
controller. Also from $[\mathcal{P}(\theta, \gamma )]^n = \mathcal{P}(\theta, n \gamma )$, series 
of identical polarization controllers may be replaced by a single polarization controller.

%%-----------------------------------------------------------
%%  Polarization Controller Diagram
\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{pdf/figure_polarization_controller_diagram.pdf}
    \caption{Diagram of a polarization controller.  Axes and strength of birefringence are adjusted by fiber holder and actuator, respectively.}
    \label{fig:polarization_controller_diagram}
\end{figure}
%%-----------------------------------------------------------


The operator $\mathcal{P}$ does not form a group in 3-sphere $S^3$ because it does not satisfy 
closure: Product of elements in $\mathcal{P}$ cannot be expressed by an element of $\mathcal{P}$. 
This point is immediately confirmed from the fact that an element $\mathcal{P}(\theta, \gamma )$ 
is a symmetric matrix but product of two symmetric matrices are not symmetric\footnote{Two 
different polarization controllers do form $\mathrm{SU}(2)$ group, by the way.}. On the other 
hand, if you plot point $\vect{y}=\mathcal{P}\vect{x}$ on Poincaré sphere, for arbitrary 
$\vect{x}$ and parameters $(\theta, \gamma)$, the point $\vect{y}$ seems to cover all over the 
sphere as illustrated in Figure \ref{fig:bloch_sphere_coverage}.  If an operator can map a point 
$\vect{x}$ to an arbitrary point $\vect{y}$, it should satisfy closure of operations.  What the 
hell is going on?


The problem lies in the choice of geometry. $\mathcal{P}$ does not form $S^3\cong \mathrm{SU}(2)$ 
but it spans fully in quotient space $S^2 \cong \mathrm{SU}(2)/\mathrm{U}(1)$. This is confirmed 
by Cartan's covering theorem\cite{GilmoreTextbook} and generator of $\mathcal{P}$ span only two 
dimensional Lie algebra. This is directly related to the dropping of $e^{ i \gamma }$ factor in 
\eqref{eq:jones_vector} when $x$ is mapping to Poincaré sphere.

Another way to confirm that $\mathcal{P}$ covers entire Poincaré sphere is to view it as rotation 
in $\mathbb{R}^3$. In terms of \eqref{eq:three_dimensional_rotation},
%
\[
    \mathcal{P}(\theta, \gamma ) = R_y(\theta ) R_z(\gamma ) R_y(\theta )^\dagger = R_{z \text{, new}} (\gamma ).
\]
%
Geometrically $R_{z \text{, new}}(\gamma )$ means rotation $\gamma$ about a new axis 
$z_\text{new}$ which is tilted from $z$-axis by $y$-axis rotations. With inspection, we see this 
operation maps a point $\vect{x}$ to an arbitrary point on entire Poincaré sphere.





%%===================================================================
%%      Polarization Control
%% ====================================================================
\section{Polarization Control For Perfect Combining}
%
%% ---------------------------------------------------------
\subsection{Lowest-Loss Mode Selection As A Guiding Principle}
%%
Inspired by the selection dynamics in coupled fiber structure presented in Chapter 
\ref{chap:dynamics}, we are going to discuss importance of polarization control in coupled fiber 
laser system by assuming the lowest-loss selection rule still holds. Thus, we only deal with 
steady states and select the state with the lowest loss of light from cavity as the system's 
choice, instead of a specific dynamical model for polarized light.


%% ---------------------------------------------------------
%%
\subsection{Steady States in Two Coupled Fiber Lasers}
%%
We let the transfer operator for the a round trip of light in the upper-left arm as 
$\mathcal{T}_1$, lower-left arm as $\mathcal{T}_2$, and upper-right arm as $\mathcal{T}_R$, as in 
Figure ??. And let $\vect{x_1}$be the Jones vector of right-going wave at the boundary of 
upper-left arm and the coupler and $\vect{x_2}$ be at the lower-left arm and the coupler. Assuming 
the coupler does not discriminate polarization state of light, we impose the coupling matrix to 
Jones-vector fields in the same manner as scalar fields.  That is, fields before and after the 
coupler, $(\vect{x_1},\vect{x_2})$ and $(\vect{y_1}, \vect{y_2})$, are related by
%%
\[
\begin{pmatrix}
   \vect{y_1} \\
   \vect{y_2}
\end{pmatrix}
=
\begin{pmatrix}
    \cos  p & i \sin  p \\
   i \sin  p & \cos  p 
\end{pmatrix}
%-----------------
\begin{pmatrix}
    \vect{x_1} \\
    \vect{x_2}
\end{pmatrix}.
\]
%
%%-------------------------
%%  
\begin{figure}[h]
    \centering
    \includegraphics[width=13cm]{pdf/figure_polarization_twolaser_diagram.pdf}
    \caption{Transfer matrices $\mathcal{T}_1$, $\mathcal{T}_2$, and $\mathcal{T}_R$ 
    are defined for a round trip of light in each arm.}
    \label{fig:polarization_twolaser_diagram}
\end{figure}
%%--------------------------
With transfer matrices defined in the figure \ref{fig:polarization_twolaser_diagram}, 
fields must satisfy the following in stationary state.
%
\begin{align*}
    \begin{pmatrix}
        \vect{x_1} \\
        \vect{x_2}
    \end{pmatrix}
    =
    \begin{pmatrix}
        \mathcal{T}_1 & 0 \\
           0             &  \mathcal{T}_2
    \end{pmatrix}    
    \begin{pmatrix}
        \cos p \;    I_2   & i \sin  p \; I_2  \\
       i \sin  p \;   I_2  & \cos  p  \;  I_2
    \end{pmatrix}    
    \begin{pmatrix}
        \mathcal{T}_R & 0 \\
           0             &  0
    \end{pmatrix}
    \begin{pmatrix}
        \cos  p  \; I_2    &  i \sin  p   \;  I_2   \\
       i \sin  p   \; I_2   &   \cos  p   \;  I_2  
    \end{pmatrix}
    \begin{pmatrix}
        \vect{x_1} \\
        \vect{x_2}
    \end{pmatrix}    
\end{align*}
where $I_2$ is $2 \times 2$ identity matrix. This equation is simplified to 
\begin{align*}
    \vect{x_1} &= \cos p \; \mathcal{T}_1 \mathcal{T}_R ( \vect{x_1} \cos p + i \vect{x_2} \sin p ), \\
    %%-----
    \vect{x_2} &= i \sin p \; \mathcal{T}_2 \mathcal{T}_R ( \vect{x_1} \cos p + i \vect{x_2} \sin p ).
\end{align*}
%
Rearrangement of the equations yields $\vect{x_2}= i \tan p \; \mathcal{T}_2 \mathcal{T}_1^{-1} 
\vect{x_1}$. And $\vect{x_1}$ is determined from the eigenvalue-looking problem.
%
\begin{equation}
    \vect{x_1} = \mathcal{T}_1 \mathcal{T}_R
    \left( \cos^2 p \; \mathcal{I} - \sin^2 p \; \mathcal{T}_2
           \mathcal{T}_1^{-1} \right) \vect{x_1}
    \label{eq:polarization_twolaser_steadystate_field}
\end{equation}
%
Here I said eigenvalue-like because Jones-vector field $\vect{x_i}$ and gain $G_i$, implicit in the transfer matrices $\mathcal{T}_i$ ($i=1,2$), are also related by equations for light-atom interactions. Under wild assumption that polarization of light does not make any difference
in the interaction, we would have a function
\begin{equation}
    \norm{\vect{x_i}}^2 = f_i (G_i) \qquad \text{for $i = 1, 2$}
    \label{eq:polarization_twolaser_steadystate_gain}
\end{equation}
where $f(G)$ is a monotonically decreasing function $df/dG<0$ and $f>0$. Subscript is added to $f$ because the function depends on the amount of pumping to each fiber. 
In the steady state, coherence of beam combining is evaluated from addition efficiency $\eta$.
\begin{equation}
    L =  \norm{ i \; \vect{x_1} \sin p + \vect{x_2} \cos p }
      = | \sin p | \; \norm{ ( \mathcal{I} + \mathcal{T}_2 \mathcal{T}_1^{-1} ) \vect{x_1} }
    \label{eq:polarization_twolaser_loss}
\end{equation}
%
Note that loss is independent of $\mathcal{T}_R$, meaning that birefringence and polarization 
controls in the arm 3 does not affect coherence of combining.

With the assumption of lowest-loss principle, we are going to deal with an optimization problem 
which minimizes the loss \eqref{eq:polarization_twolaser_loss} under the constraints 
\eqref{eq:polarization_twolaser_steadystate_field} and 
\eqref{eq:polarization_twolaser_steadystate_gain}. 
In addition to system's autonomous tuning of the best mode, 
we have control over polarization controllers and tune them for the best performance. 




%% ----------------------------------------------------------------
\subsection{Transfer Matrices}
%%
Transfer operator is composed of translation, birefringence, amplification by stimulated emission, 
and loss by reflection. Translation may be regarded as overall phase rotation of Jones-vector 
state so it is expressed by factor $e^{ i \beta L}$ where $\beta$ is propagation constant and $L$ 
is optical path length. Birefringence in fiber is equivalent to a polarization controller 
$\mathcal{P}(\theta, \gamma)$ with uncontrollable fixed parameters $(\theta, \gamma)$. Loss of 
light is assumed to occur only at the reflecting end and other loss, such as transmission and 
connector loss, are not considered. Partial reflection occurs only at arm 3 in the diagram so the 
loss is represented by simple multiplication of positive constant $r < 1$. Amplification is due to 
stimulated emissions in doped fiber region in the upper-left and lower-left arms.  For simplicity 
of argument, amplification is constant and irrelevant to polarization state. 
\footnote{Polarization actually matters in reflection and amplification of light. But let us 
forget the contribution with caution for the simplicity of argument.} With these simplifications, 
everything except birefringence matrices are multiplication constants. Thus, transfer matrices for 
three arms are given by
%%
\begin{align*}
    \mathcal{T}_1 &= e^{G_1} e^{i \beta L_1} \prod_{i=1} \mathcal{P}_{1i},  \\
    \mathcal{T}_2 &= e^{G_2} e^{i \beta L_2} \prod_{j=1} \mathcal{P}_{2j},  \\
    \mathcal{T}_R &= r e^{i \beta L_R} \prod_{k=1} \mathcal{P}_{R k},
\end{align*}
%%
where multiple $\mathcal{P}$ are introduced to include both intrinsic birefringence as well as 
manual insertion of polarization controllers.  We are not going to adjust optical lengths 
$(L_1, L_2, L_R)$ but assume that they are incommensurate.
    

%%=======================================================
%%     Poincare/Bloch sphere coverage
\begin{figure}[h!]
    \centering
    \includegraphics[width=7cm]{pdf/figure_bloch_sphere_coverage.pdf}
    \caption{Points $\mathcal{P}(\theta, \gamma) x$ on Poincaré sphere is illustrated
     with $x = (0.6, 0.8 e^{1.0 i})$ and varying $\theta$ continuously in 
     $[0, \pi)$ and $\gamma$ discretely in $\gamma = 0.1, 0.2, \dotsc, 1.5$.}
    \label{fig:bloch_sphere_coverage}
\end{figure}
%%=======================================================



%-----------------------------------------------------
\subsection{Perfect Combining in Two Fiber Lasers}
\label{sub:polarization_twolaser_combining}
%
We are going to investigate conditions for completely coherent combining within 
Jones-vector field framework. Scalar-field version was presented in Section 
\ref{sub:perfect_combining_solution}. With the lowest-loss mode selection scheme, 
we would like to choose steady state solutions which satisfy
%
\[
  \vect{x} = \mathcal{T}_1 \mathcal{T}_R 
  (\cos^2 p \; \mathcal{I} - \sin^2 p \; \mathcal{T}_2 \mathcal{T}_1^{-1}) \vect{x}
\]
%
such that the loss $L$ is minimized.
%
\[
    L = |\sin  p| \left\| (\mathcal{I} + \mathcal{T}_2 \mathcal{T}_1^{-1} ) \vect{x} \right\|
\]
%
Inspired by the experimental observation we seek the perfect combining which give $L=0$. Then following two equations must be satisfied.
%
\begin{align}
    \mathcal{T}_2 \mathcal{T}_1^{-1} \vect{x} = - \vect{x}  
    \label{eq:polarization_perfect_combining1} \\
    \mathcal{T}_1 \mathcal{T}_R \vect{x} = \vect{x}         \label{eq:polarization_perfect_combining2}
\end{align}
%
When two different operators share the same eigenvector $\vect{x}$, they must commute $\left[ \mathcal{T}_2 \mathcal{T}_1{}^{-1}, \mathcal{T}_1\mathcal{T}_R \right] = 0$. This leads to 
%
\begin{equation}
    \mathcal{T}_1 \mathcal{T}_R \mathcal{T}_2 = \mathcal{T}_2 \mathcal{T}_R \mathcal{T}_1
    \label{eq:polarization_twolaser_commutation}
\end{equation}
%
Let $\mathcal{T}_R \mathcal{T}_2 = \mathcal{T}_2 \mathcal{T}_R = \alpha \mathcal{I}$. Substituting 
it back to \eqref{eq:polarization_perfect_combining1}, we immediately have $\alpha = -1$.  This 
operator identity is possible by tuning a polarization controller $\mathcal{P}_a$ attached to 
$\mathcal{T}_2$ arm. Explicit form of $\mathcal{T}_2 \mathcal{T}_R = - \mathcal{I}$ is
%
\begin{equation}
    r e^{G_2}e^{ i \beta (L_2 + L_R)} \prod_j \mathcal{P}_j \prod_k \mathcal{P}_k = -\mathcal{I}
    \label{eq:polarization_twolaser_condition}
\end{equation}
%
which determines polarization controller setting of $\mathcal{P}_a$
%
\[
    \mathcal{P}_a = 
    \left( \prod_{j=1}^{a-1} \mathcal{P}_j \right)^{-1} 
    \left( \prod_k \mathcal{P}_k \right)^{-1}
    \left( \prod_{j=a+1}^{a-1} \mathcal{P}_j \right)^{-1}
\]
%
With the property $[\mathcal{P}(\theta, \gamma )]^{-1} = \mathcal{P}(\theta, -\gamma )$, 
$\mathcal{P}_a$ is represented as a product of elements of $\mathcal{P}$. Because of the closure 
of $\mathcal{P}$ on Poincaré sphere discussed in previous section, such $\mathcal{P}_a$ always 
exists. \eqref{eq:polarization_twolaser_condition} also requires relation between steady state 
gain and propagation constants.
%
\begin{align}
    & G_2=-\log  r  \nonumber \\
    & e^{ i \beta (L_2+L_R)} = -1  \label{eq:polarization_twolaser_phaserelation2R}
\end{align}
%
Also, substituting the expression for $\mathcal{T}_2$ into 
\eqref{eq:polarization_perfect_combining1}, we have
\begin{align}
    G_1 = G_2 = - \log  r \\
    %%
    e^{ i \beta (L_2 - L_1)} = -1 \label{eq:polarization_twolaser_phaserelation12}\\
    %%
    \left( \prod_i \mathcal{P}_i \right) 
    \left( \prod_j \mathcal{P}_j \right)^{-1} \vect{x} = \vect{x}
    \label{eq:polarization_twolaser_eigenequation}
\end{align}
%
Gain relation agrees with steady state of scalar-field dynamical model and it is automatically 
satisfied by the system. Phase relation is also autonomously satisfied by the system due to 
lowest-loss mode selection rule.  Here I am assuming $(L_1, L_2, L_R)$ are incommensurate so both 
\eqref{eq:polarization_twolaser_phaserelation12} and 
\eqref{eq:polarization_twolaser_phaserelation2R} are satisfied quasi-periodically to arbitrary 
precision. \eqref{eq:polarization_twolaser_eigenequation} shows that the eigenvector $\vect{x}$ is 
determined for given birefringence in arm 1 and 2.  Thus, even if polarization controller attached 
to arm 1 is varied, the perfect combining is unchanged although the corresponding
eigenstate is changed. This outcome agrees with the experimental observation (bottom of Figure 
\ref{fig:willray_coherence_vs_polarization}) that lossless addition becomes independent of the 
polarization control.



When polarization controller in arm 2 is \emph{not} adjusted to satisfy the relation 
\eqref{eq:polarization_twolaser_commutation}, perfect condition is still achieved by making an 
operator identity $\mathcal{T}_2 = -\mathcal{T}_1$. Due to closure of $\mathcal{P}$ on Poincaré 
sphere, the operator identity is always possible by adjusting polarization controller in arm 1 to 
cancel out birefringence in arm 1 and 2.  But polarization controller setting in arm 1 is now 
dependent on polarization in arm 2. Therefore perfect coherent combining breaks as you vary the 
polarization in the other arm. This explains the other experimental observation (top two graphs in 
Figure \ref{fig:willray_coherence_vs_polarization}) that combining efficiency is dependent on the 
polarization setting.



%%---------------------------------------------------------
%%
\subsection{Combining in Larger Array}
%
In $N=2$, we have confirmed our experimental observation and conclude that only one polarization 
controller is needed to achieve coherent beam combining. Then how many polarization controllers 
are needed in larger array? Straightforward extension of our argument suggests that $(N-1)$ number 
of polarization controllers are necessary for $N$ linear fiber array to achieve coherent 
combining. Let $c_{ij}$ be $(i,j)$ element of coupling matrix and $a$ as index of the reflective 
fiber. In the similar manner as \eqref{} the steady-state equation is,
%
\[
    \vect{x_n} = \sum_{i} \mathcal{T}_n  c_{ni} \mathcal{T}_R \delta_{ia} \sum_{j}\delta_{ij}  \sum_{k} c_{jk} \vect{x_k} 
\]
%
This is simplified to
%
\[
    \frac{1}{c_{na}}  \mathcal{T}_n^{-1}   \vect{x_n}  =  \mathcal{T}_R  \sum_{k} c_{ak} \vect{x_k}  
\]
%
Rewriting the steady-state equation with 
$\vect{x_n} = \frac{c_{n a}}{c_{1a}}  \mathcal{T}_n \mathcal{T}_1^{-1}  \vect{x_1}$, 
we have
%
\[
    \vect{y} = \mathcal{T}_R \sum_{k} ( c_{ak}   c_{ka}  \mathcal{T}_k ) \vect{y}
\]
%
where new vector $\vect{y}$ is introduced by $\vect{y} \equiv \mathcal{T}_1^{-1} \vect{x}$. 
And loss at each angle-cleaved end, indexed $ i \neq a$, is
%
\[
    L_i  \equiv    \left\|  \sum_{k}  c_{ik}  \vect{x_k} \right\|  
 =  \frac{1}{\left|c_{1a}\right|}   \left\| \sum_{k} ( c_{ik} c_{ka} \mathcal{T}_k ) 
       \vect{y} \right\| \qquad \text{for $i \neq a$}
\]
%
Inspired by our scalar-field argument (Section \ref{sub:perfect_combining_solution}) and two 
polarized fibers (Section \ref{sub:polarization_twolaser_combining}), we see that following 
operator identity must hold so that the steady state solution have zero loss.
%
\[
  \mathcal{P}_{1\text{ all}} = \mathcal{P}_{2 \text{ all}} = \dotsb = \mathcal{P}_{N \text{ all}}
\]
%
where $\mathcal{P}_{ i \text{ all}}$ is product of birefringence effects $\mathcal{P}$ in fiber 
arm $i$.  This equality requires $(N-1)$ number of polarization controllers. It is also required 
that phase relation satisfy $e^{ i \beta L_k} c_{ka} = c_{ka}^* $for all $k$, in the same way as 
scalar-field calculation in Section \ref{sub:perfect_combining_solution}. As long as lengths $L_i 
(i = 1, 2, \dotsc, N)$ are incommensurate, the equality is quasi-periodically satisfied to 
arbitrary precision for all $ k = 1, 2, \dotsc, N$ with proper choice of $\beta$.  As $N$ grows, 
however, convergence becomes slower due to a vernier effect, which was discussed in scalability of 
combining (Section \ref{sec:power_scaling}).




%%----------------------------------------------------
%%
\subsection{Conclusion}
%%
In summary, our experimental observation, relation between coherent combining and polarization 
controller setting, has been explained in terms of Jones-vector extension of our model and the 
lowest-loss mode selection rule. The argument is extended to larger array and it is found that 
$(N-1)$ polarization controllers are required in $N$ (Michelson-interferometer-type) fiber array.

Our conclusion is consistent with experiments by Shirakawa\cite{Shirakawa2005} and 
Sabourdy\cite{Sabourdy2003}. Self-organization without a polarization controller would require 
intrinsic discrimination of polarized light in the system, in  form of polarization-dependent gain 
or loss.