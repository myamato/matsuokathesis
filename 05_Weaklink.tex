% !TEX root = __main.tex

\chapter{Multi-Output System With Inhomogeneous Lasers} 
\label{chap:weaklink}

%% ---------------------------------------------------------
%%              Introduction
%% ---------------------------------------------------------
\section{Synchronization of Inhomogeneous Oscillators}
In coupled oscillators, it is generally true that nearly-identical oscillators tend to get 
synchronized and inhomogeneity of oscillator elements breaks the synchronization. In Kuramoto 
phase oscillator model, for example, identical oscillators are synchronized easily with any 
positive coupling constant but it gets harder (stronger coupling is required) to synchronize 
inhomogeneous oscillators\cite{Strogatz2000}.
%%
Coherent beam combining discussed in Chapter~\ref{chap:dynamics} is different from the phase 
oscillator models because amplitude dynamics matters most. However, homogeneity of oscillator 
elements is still required for successful combining. As demonstrated in Section 
\ref{sub:perfect_combining_solution}, even pumping is part of the requirement for fully-coherent 
addition of lasers.

Tsygankov and Wiesenfeld\cite{Tsygankov2006}, however, have shown that new type of synchronization 
of coupled oscillators is possible by exploiting parameter mismatches of oscillator elements. They 
took array of Hopf oscillators, coupled them in special geometry that active (over-threshold) 
elements sandwich inactive (under-threshold) elements, and then achieved robust synchronization. 
As an example of the ON-OFF-ON geometry, Figure~\ref{fig:weaklink_geometry} shows placement of 37 
elements (24 active and 13 inactive elements) in ring arrangement. This type of synchronization, 
called ``weaklink synchronization'', is thought to exist in many class of coupled oscillators 
where amplitude dynamics is crucial.
%
\begin{figure}[h!]
    \centering
    \includegraphics[width=5cm]{pdf/weaklink_geometry.pdf}
    \caption{Arrangement of 37 Hopf oscillators for weaklink synchronization. Black and gray circles represent over-threshold and sub-threshold oscillators, respectively. Retrieved from \cite{Tsygankov2006}.}
    \label{fig:weaklink_geometry}
\end{figure}
%

Motivated by the idea, we explore the possibility of a new multi-output beam combining scheme by 
imposing inhomogeneous energy input to fibers. We choose $N=3$ fibers to realize the simplest 
ON-OFF-ON geometry: Outer laser pump are set over-threshold and the middle laser is 
under-threshold so that the middle laser does not lase if uncoupled. To fully investigate the 
impact of inhomogeneous pumping to laser elements, inhomogeneous optical loss is also treated.



%% =========================================================
%%              Experiment
%% =========================================================
%%
\section{Experiment}
%% ---------------------------------------------------------
%%   Set up
%%
\subsection{Experimental Set Up}
A schematic diagram of the three-laser array is shown in Figure~\ref{fig:weaklink_schematic}. 
%
\begin{figure}[h!]
    \centering
    \includegraphics[width=12cm]{pdf/experiment_schematic_diagram.pdf}
    \caption{Schematic of the three laser array.} 
    \label{fig:weaklink_schematic}
\end{figure}
%
Each laser element is constructed of Yb-doped fiber pumped by grating-stabilized 976 nm diode 
lasers. A fiber loop mirror spliced onto one end of each doped fiber provides nearly 100\% 
broadband back reflection; a flat-cleave on the other end reflects only 4\% of the generated 
light. An asymmetric coupling is formed by concatenating two $2 \times 2$ 
fused fiber couplers with an 80/20 power-splitting ratio. 
The three output facets are aligned in a grooved substrate with a 
spacing of 250 $\mu$m; a CCD camera captures the far-field interference pattern at a frame rate of 
77 Hz. Tap couplers are placed in each of the output fiber arms to monitor the individual powers 
and optical spectrum. This array emits over a 3 nm bandwidth near 1030 nm and reaches lasing 
threshold once 40 mW of pump power is applied to any one of the elements.

The middle laser pump is tuned from 0 to 2.5 times lasing threshold while the outer lasers are 
fixed at 10\% above threshold. We investigated this inhomogeneously-pumped system with homogeneous 
and inhomogeneous loss. Uneven loss is realized by applying additional cavity loss to the middle 
fiber by compressing a tight coil of fiber so that over 90\% of the incoming light is lost.


\subsection{Inhomogeneity of Laser Elements and Coherence}
Coherence of three laser beams is evaluated by visibility $V$ of fringes in far-field 
interference pattern. Here, visibility $V$ is determined from the spatial maximum and the 
minimum of adjacent irradiances by\cite{HechtTextbook}
\[
  V = \frac{I_\mathrm{max} - I_\mathrm{min}}{I_\mathrm{max} + I_\mathrm{min}}.
\]
Output power of three lasers and average visibility are shown in 
Figure~\ref{fig:experiment_visibility} for homogeneous and inhomogeneous losses.
%
% -------------------------------
%   Visibility vs Pump
\begin{figure}
    \centering
    \begin{subfigure}[h]{5cm}
        \centering
        \includegraphics[width=5cm]{pdf/experiment_visibility_EvenLosses.pdf}
        \caption{Even loss.}
    \end{subfigure}  
    ~  
    \begin{subfigure}[h]{5cm}
        \centering
        \includegraphics[width=5cm]{pdf/experiment_visibility_AddedLoss.pdf}
        \caption{Uneven loss.}
    \end{subfigure}      
    \caption{Average vibisility vs middle laser pump. Outer laser pumps are fixed at 1.1x threshold.  Uneven losses indicates additional heavy loss to the middle fiber.}
    \label{fig:experiment_visibility}
\end{figure}
%
%
In both cases three laser beams have different output powers depending on the pump to the middle 
fiber, as in Figure~\ref{fig:experiment_power}. When loss is even, output power from the middle 
fiber is smaller than the power from outer fibers when the middle fiber is under-pumped, and the 
power from the middle fiber exceeds the rest when it is over-pumped. When loss is uneven, output 
power is mainly from two outer fibers regardless of the pump.
%
%
% -------------------------------
%   Power vs Pump
\begin{figure}
    \centering
    \begin{subfigure}[h]{7cm}
        \centering
        \includegraphics[width=7cm]{pdf/experiment_PowerVsPump_EvenLosses.pdf}
        \caption{Even loss}
        \label{fig:experiment_visibility_EvenLosses}
    \end{subfigure}  
    ~  
    \begin{subfigure}[h]{7cm}
        \centering
        \includegraphics[width=7cm]{pdf/experiment_PowerVsPump_AddedLoss.pdf}
        \caption{Uneven loss}
        \label{fig:experiment_visibility_AddedLoss}        
    \end{subfigure}      
    \caption{Output powers vs middle laser pump. 
    Outer laser pumps are fixed at 1.1x threshold. 
    Uneven losses indicates additional heavy loss to the middle fiber.}
    \label{fig:experiment_power}
\end{figure}
%
%




% -------------------------------
%   visibility histogram: experiment
\begin{figure}[h!]
    \centering
    \includegraphics[width=11cm]{pdf/experiment_visibility_histograms.pdf}
    \caption{Distribution of experimental visibilities with extra loss to the middle laser. when 
    cavity loss where middle laser has applied cavity losses with pump set to (a) 10\% above and 
    (b) 6\% below lasing threshold.}
    \label{fig:visibility_histogram}
\end{figure}







%% =============================================================
%%    Simulation
%% =============================================================
\section{Simulation}
\subsection{Model}
Simulation of the three-laser system uses the three-level multi-axial
 mode model introduced in Chapter~\ref{chap:models}. 
\begin{align*}
    E_n^{(m)} (t+T)
    &= 
    e^{G_n(t) + i \phi_n^{(m)}} 
    \sum_{j=1}^N \left( \mathcal{U}^T \right)_{n j}  r_j e^{i \psi_j^{(m)} }
    \sum_{k=1}^N \mathcal{U}_{j k} E_k^{(m)} (t)  + \xi_n^{(m)} (t),  \\
    %%
    G_n(t+T) 
    &=  
    G_n(t) + \epsilon \left[ x_n W_\mathrm{th} (G_\text{tot} - G_n(t)) - (G_\text{tot} +  G_n(t)) 
                       - 2 \left(1 - e^{-2 G_n(t)}\right) \sum_m |E_n^{(m)} (t)|^2   \right] 
\end{align*}
The map model is almost identical to the single-output system in Chapter~\ref{chap:dynamics}, 
except additional phase parameters $\psi_j^{(m)}$ are introduced to represent phase shift during 
the propagation in the right arms (\emph{c.f.} Figure~\ref{fig:weaklink_schematic}). The model is 
treated as multi-output system by counting laser emission from all flat-cleaved ends.

The coupling matrix $\mathcal{U}$ is constructed from product of two 80/20 coupling matrices, 
which couple fiber \#1 to \#2 and \#2 to \#3. With a 2x2 coupler representation in Section \ref{}, 
we have
\[
    \mathcal{U} = 
    \begin{pmatrix}
        \sqrt{0.8}& i \sqrt{0.2} & 0            \\
        i 0.4     &    0.8       & i \sqrt{0.2} \\
        -0.2      &  i 0.4       &   \sqrt{0.8}
    \end{pmatrix}.
\]

Reflection coefficients are set to $(r_1, r_2, r_3) = (0.2000, 0.2005, 0.2010)$ to simulate 
even-loss case and $(r_1, r_2, r_3) = (0.200, 0, 0.201)$ to simulate uneven-loss, which was 
realized by additional heavy loss to the middle fiber. Here losses are slightly detuned even for 
even-loss to avoid artifacts arising from the symmetry. Other parameters are set so that 
$G_\text{tot} = 2.3$ $\epsilon = 10^{-3}$. This choice of $\epsilon$ is larger than realistic 
value, $\epsilon \sim O(10^-4)$, but this choice only speeds up the convergence without altering 
the CW state as long as it does not incurs numerical instability.

Pump to outer fibers (Laser \#1 and \#2) is fixed at 10\% above the threshold of OFF to CW 
transition; $x_1 = x_3 = 1.1$. Pump to the middle fiber (Laser \#2) is swept from $x_2=0$ to $x_2 
= 2.5$ to replicate the experiment.



\subsection{Far Field Pattern}
Far-field interference pattern is obtained from the state of the iterative map model by following 
procedures. First, electric fields $E_{\mathrm{facet, } i}^{(m)}$ at each exiting facet $i=1,2,3$ 
is determined from the state variable $E_i^{(m)}$ (the field at the reference point) by transfer 
matrix $\mathcal{S}$ of  wave propagation through the coupler and the output arms.
\[
    E_{\mathrm{out}, i}^{(m)} = \sum_{j=1}^N \mathcal{S}_{i j} E_{j}^{(m)}
\]
where $m$ is an index for longitudinal modes. We set the coupling $\mathcal{U}$ as the 
transfer matrix and ignore propagation. Here $\sum_m |E_{\mathrm{out}, i}^(m)|^2$ for $i=1,2,3$ 
represent the powers measured by the experimental taps.

Second, a two-dimensional near-field of the array output is prepared assuming truncated Gaussian 
envelopes positioned at the three fiber core centers and with a cutoff at the core diameters. Each 
envelope inherits the amplitude and phase of the propagated electric fields.

Third, assuming Fraunhofer diffraction limit, a two-dimensional far field pattern is computed the 
near field by 2-D Fourier transformation\cite{HechtTextbook}. This is because far field  
$E_\mathrm{far}$ is proportional to the 2-D integral,
%
\[
    E_\mathrm{far}^{(m)} (x, y) \propto \int \int_\mathrm{Aperture}
        E_\mathrm{near}^{(m)} (x',y') e^{-i k (x x' + y y')/z} dx' dy',
\]
%
and this is practically computed with 2-D FFT together with change of length scale. 
Irradiance of instantaneous far field is calculated from sum of the 
field intensities of all modes,
\[
    I(x,y) = \sum_m |E_\mathrm{far}^{(m)}|^2 (x, y).
\]

Finally, time-averaged irradiance pattern is constructed from 20 far-field data sampled every 5 
iterations to mimic the camera exposure time ($\sim 100 \mathrm{\mu s}$) for each captured frame.



\subsection{Results: Visibilities}
Change in visibility for varying pump to the middle fiber is shown in 
Figure~\ref{fig:simulation_visibility_EvenLosses} and 
Figure~\ref{fig:simulation_visibility_AddedLoss} for even-loss and uneven-loss setups, 
respectively.


% -------------------------------
%   Visibility vs Pump: Simulation
\begin{figure}
    \centering
    \begin{subfigure}[h]{5cm}
        \centering
        \includegraphics[width=5cm]{pdf/simulation_visibility_EvenLosses.pdf}
        \caption{Even loss}
    \end{subfigure}  
    ~  
    \begin{subfigure}[h]{5cm}
        \centering
        \includegraphics[width=5cm]{pdf/simulation_visibility_AddedLoss.pdf}
        \caption{Uneven loss}
    \end{subfigure}      
    \caption{Average vibisility vs middle laser pump from simulation. Each circle represents each 
    realization, and size of a circle corresponds to the number of active modes. [FIXME] Take care 
    that scales of radius are different in two graphs!}
    \label{fig:simulation_visibility}
\end{figure}



% -------------------------------p
%   Power vs Pump
\begin{figure}
    \centering
    \begin{subfigure}[h]{7cm}
        \centering
        \includegraphics[width=7cm]{pdf/simulation_PowerVsPump_EvenLosses.pdf}
        \caption{Even loss}
    \end{subfigure}  
    ~  
    \begin{subfigure}[h]{7cm}
        \centering
        \includegraphics[width=7cm]{pdf/simulation_PowerVsPump_AddedLoss.pdf}
        \caption{Uneven loss}
    \end{subfigure}      
    \caption{Output powers vs middle laser pump from simulation. Outer laser pumps are fixed at 
    1.1x threshold. Uneven losses indicates additional heavy loss to the middle fiber.}
    \label{fig:simulation_power}
\end{figure}
%





%% =============================================================
%%         Discussions
%%
%% =============================================================
\section{Discussions}
%%
\subsection{Visibility, Number of Active Modes, and Mode Discrimination}

\subsection{Is There Weaklink Synchronization?}

