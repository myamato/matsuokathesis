Does nonzero visibility mean partial coherence?
===============================================

I'm going to discuss if outputs from two fibers were partially coherent or not.
Although nonzero visibility was observed in our experiments, we have to make
this point clear because even incoherent light sources can give partial
coherence in far field (van Cittert-Zernike theorem). The theorem says far-field
pattern from parallel incoherent sources is given by Fourier transform of the
source, which is mathematically equivalent to Fraunhofer diffraction pattern of
coherent light of the same intensity pattern.





No fringe pattern is observed for incoherent light source unless double slit is
placed in the optical path? [Roychoudhuri1995]



Fraunhofer diffraction: aperture field ⇔ far field

van Cittert-Zernike: intensity distribution ⇔ correlation function





















Reference
---------

C. S. Roychoudhuri, K. R. Lefebvre, SPIE 2525, 148-160 (1995)
