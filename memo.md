On Fiber Optics
===============
Aug 31, 2012 (Fri)

* I need to deliver ideas, not reproductions of equations in textbook. Just put a pointer if you need it.

* Writing does not have to be formal. [Look at Feynman's Books!] What matters is its contents. English does not matter as well if it's readable.

* Do not bombard with equations! Explain it!



What I am going to talk about here is electromagnetic fields in fibers. But I'm not going to reproduce general knowlege in fiber optics: You can find huge descriptions in textbooks of fiber optics such as Agrawal\cite{AgrawalTextbook}[^1]. What I'm going to provide is the minimum idea needed to get started with out job: Studying the dynamical systems of coherent beam combining by modeling behavior light in fibers.



<!--On plain wave approximation  -->
Light in a fiber is treated as a plane wave in our dynamical models (Chapter \ref{chap:n-to-1-combining}). On the other hand, textbooks give us three-dimensional description involving Bessel functions and their variants by solving the Helmholtz equations in cyllindrical coordinates[^2]\cite{\OkamotoMonograph}. Then how is the description simplified to plane wave?


For example electric field for step-indexed fiber is,
\[
\tilde{\vect{E}}_z (\vect{r},ω) = A(ω) F(\rho) exp(i m \phi) exp(i \beta z),
\]
where 
\[
F(\rho) = 
\begin{cases}
    J_m(p \rho) & \text{for $\rho <= a$} \\
    K_m(q \rho) & \text{for $\rho > a$} \\
\end{cases}    

\]



<!--On plain wave approximation  -->







[^1]: I guess it would be more helpful to readers to just put pointers to good references than putting my non-expert explanations. 

[^2]: Appendix A gives a neat and versatile way to get coordinates representation of Laplacian.
